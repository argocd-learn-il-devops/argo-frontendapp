FROM node:14-alpine as build

WORKDIR /src/app
COPY ./ /src/app/
RUN if [[ ! -d node_modules ]]; then npm i; fi && \
    if [ ! -d dist ] ; then npm run build; fi && \
    find . -maxdepth 1 -not -name "dist" -a -not -name "." -a -not -name ".." -exec rm -rf {} \;

FROM nginx:stable-alpine
COPY --from=build /src/app/dist /usr/share/nginx/html

# Expose port 80
EXPOSE 80